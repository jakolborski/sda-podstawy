import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WyrazeniaRegularne {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("w3schools", Pattern.CASE_INSENSITIVE); //Wzorzec
        Matcher matcher = pattern.matcher("Visit W3Schools!");
        //boolan matchFound = matcher.matches();
        //pokaże czy pokrwa całe wyrażenie, ale żeby pokazało to trzeba Pattern pattern = Pattern.compile(".*w3schools.*", Pattern.CASE_INSENSITIVE)
        boolean matchFound = matcher.find();
        if (matchFound) {
            System.out.println("Match found");
        } else {
            System.out.println("Match not found");
        }

        String line = "This order was placed for QT3000! OK?";
        String pattern1 = "(.*)(\\d+)(.*)";

        Pattern r = Pattern.compile(pattern1);

        Matcher m = r.matcher(line);
        if (m.find()) {
            System.out.println("Found value: " + m.group(0));
            System.out.println("Found value: " + m.group(1));
            System.out.println("Found value: " + m.group(2));
        } else {
            System.out.println("No match");
        }
    }
}
