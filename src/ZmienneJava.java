public class ZmienneJava {
    static int myGlobalVariable = 111;
    static Integer myGlobalVariableInteger = 97;

    public static void main(String[] args) {
        float liczbaZmiennoprzecinkowa = 12.1f;
        Float liczbaZmiennoprzecinkowaFloat = 12.1f;
        double liczbaTypuDouble = 12.2;
        System.out.println(liczbaZmiennoprzecinkowa);
        System.out.println(liczbaZmiennoprzecinkowaFloat);
        System.out.println(liczbaTypuDouble);
        System.out.println(myGlobalVariable);
        myGlobalVariable = 100;
        final int secondGlobalVariable;
        secondGlobalVariable = 10;
        System.out.println(myGlobalVariable);
        System.out.println(secondGlobalVariable);
        innaMetoda();

        boolean myFalseValue = false;
        boolean myTrueValue = true;
        boolean myBooleanValue = myFalseValue && myTrueValue;
        System.out.println(myBooleanValue);
        char r = 'r';
        System.out.println(r);
        String mojTekst = "Tutaj jest mój tekst";
        System.out.println(mojTekst);
    }

    public static void innaMetoda() {
        int innaZmienna = 0;
        myGlobalVariable = 10;
        System.out.println(innaZmienna);
        System.out.println(myGlobalVariable);
    }
}