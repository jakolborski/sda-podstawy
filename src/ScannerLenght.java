import java.util.Scanner;

public class ScannerLenght {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Wpisz dowolny tekst:");
        String textLine = scan.nextLine();

        System.out.println("Ilość znaków: " + textLine.length());

        int spaceNumber = 0;
        for (int i = 0; i < textLine.length(); i++)
            if (textLine.charAt(i) == ' ') {
                spaceNumber++;
            }
        System.out.println("Ilość spacji: " + spaceNumber);
    }
}
