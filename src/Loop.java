public class Loop {
    public static void main(String[] args) {
        for (int i = 0; i <= 100; i++) {
            if (i % 2. == 0) {
                System.out.println("Liczba parzysta " + i);
            } else {
                System.out.println("Liczba nieparzysta " + i);
            }
        }
        int j = 0;
        while (j <= 100) {
            System.out.println("Liczba j wynosi " + j);
            ++j;
        }
        int k = 0;
        do {
            System.out.println("Liczba k wynosi " + k);
            k++;
        } while (k < 10);
        int l = 0;
        while (l <= 100) {
            System.out.println("Liczba l wynosi " + l);
            if (l == 33) {
                break;
            }
            l++;
        }
        for (int m = 0; m <= 10; m++) {
            if (m == 8) {
                continue;
            }
            System.out.println("Liczba m wynosi " + m);
        }
    }
}