import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Grupowanie {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("\\D+(?<kwotaKredytu>\\d+)\\D+(?<odsetki>\\d+).*");
        Matcher matcher = pattern.matcher("Mam kredyt 3000 zl odsetki to 200 zl");
        boolean matchFound = matcher.matches();
        if (matchFound) {
            System.out.println(matcher.group(0));
            System.out.println(matcher.group(1));
            System.out.println(matcher.group(2));
            System.out.println(matcher.group("kwotaKredytu"));
            System.out.println(matcher.group("odsetki"));
        } else {
            System.out.println("Match not found");
        }
    }
}
