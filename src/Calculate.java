import java.util.Scanner;

public class Calculate {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("\nWpisz pierwszą liczbę: ");
        Integer usersNumber = Integer.parseInt(scan.nextLine());

        System.out.println("\nWpisz działanie: ");
        String znak = scan.nextLine();

        System.out.println("\nWpisz drugą liczbę: ");
        Integer usersNumber2 = Integer.parseInt(scan.nextLine());
        System.out.println();

        if (znak.equals("+")) {
            System.out.println("Wynik: " + Math.addExact(usersNumber, usersNumber2));
        } else if (znak.equals("-")) {
            System.out.println("Wynik: " + Math.subtractExact(usersNumber, usersNumber2));
        } else if (znak.equals("*")) {
            System.out.println("Wynik: " + Math.multiplyExact(usersNumber, usersNumber2));
        } else if (znak.equals("/")) {
            System.out.println("Wynik: " + Math.floorDiv(usersNumber, usersNumber2));
        } else {
            System.out.println("Wpisz poprawny symbol");
        }
    }
}
