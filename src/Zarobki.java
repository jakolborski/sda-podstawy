public class Zarobki {
    public static void main(String[] args) {
        double zarobki = 100000;
        if (zarobki <= 2000) {
            System.out.println("Podatek wynosi 0zł");
        } else if (zarobki <= 4000) {
            System.out.println("Podatek wynosi: " + zarobki * 0.02 + "zł");
        } else {
            System.out.println("Podatek wynosi: " + zarobki * 0.04 + "zł");
        }
    }
}
