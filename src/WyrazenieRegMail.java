import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WyrazenieRegMail {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("\nWpisz swój mail: ");
        String mail = scan.nextLine();
        System.out.println("\nWpisz swój adres: ");
        String adres = scan.nextLine();

        Pattern pattern = Pattern.compile("\\w+@\\w+\\.[a-z]{2,3}");
        Matcher matcher = pattern.matcher(mail);
        boolean matchFound = matcher.matches();
        if(matchFound) {
            System.out.println("Mail poprawny");
        } else {
            System.out.println("Mail niepoprawny");
        }
        Pattern pattern1 = Pattern.compile("ul\\..*\\d+/\\d*");
        Matcher matcher1 = pattern1.matcher(adres);
        boolean matchFound1 = matcher1.matches();
        if(matchFound1) {
            System.out.println("Adres poprawny");
        } else {
            System.out.println("Adres niepoprawny");
        }
    }
}