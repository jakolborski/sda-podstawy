public class Operatory {

    public static void main(String[] args) {
        Integer x=10;
        Integer y=-2;
        System.out.println(Integer.compare(x,y));
        System.out.println(x.equals(y));

        System.out.println(x);
        //x=x+y
        x+=y;
        System.out.println(x);
        x*=y;
        System.out.println("Po mnożeniu: " + x);
        x/=y;
        System.out.println(x);

        System.out.println(3%2);
        int i=0;
        //System.out.println(i+1); to samo co (++i), może być też z minusem
        System.out.println(i++);

        System.out.println(!true);

        //konwersja niejawna się tutaj odbywa
        char t='7';
        int z= t;
        System.out.println(z); //55 to Unicode dla 7

        String zmienna="100";
        Integer liczba=Integer.valueOf(zmienna);
        System.out.println(liczba);

        //konwersja jawna
        double liczba1 = 3.76;
        int m= (int) Math.round(liczba1);
        System.out.println(m);
    }
}
