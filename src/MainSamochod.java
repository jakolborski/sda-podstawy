public class MainSamochod {

    public static void main(String[] args) {
        Samochod samochod = new Samochod(); //To jest konstruktor bezargumntowy, jeśli w nawiasie jest coś np, (String marka, etc.) to jest argumentowy
        samochod.setMarka("BMW");
        samochod.setModel("E23");
        samochod.setRokProdukcji(1982);

        String samochodMarka = samochod.getMarka();
        String samochodModel = samochod.getModel();
        int samochodRokProdukcji = samochod.getRokProdukcji();
        System.out.printf("%s %s, %s", samochodMarka, samochodModel, samochodRokProdukcji);
        System.out.println();
        //final mówi że nie da się zmienić zmiennej samochod1
        final Samochod samochod1 = new Samochod("BMW", "E46", 2002 );
        System.out.println(samochod1.toString());

        Samochod samochod2 = new Samochod("BMW", "F20", 2014);
        System.out.println(samochod2.toString());
    }
}
