public class Samochod {

    String marka;
    String model;
    int rokProdukcji;

    // Jeśli w MainSamochod wywołujemy oba konstruktory to musi być tutaj też bezargumentowy
    Samochod(){
    }

    Samochod (String marka, String model, int rokProdukcji) {
        this.marka = marka; //Tutaj jest to parametr
        this.model = model; //fioletowe to pole klasy a niebieskie to parametr
        this.rokProdukcji = rokProdukcji;
    }

    //To można dodać poprzez alt+ins, Getter and Setter
    void setMarka (String marka) {
        this.marka = marka;
    }
    void setModel (String model) {
        this.model = model;
    }
    void setRokProdukcji (int rokProdukcji) {
        this.rokProdukcji = rokProdukcji;
    }
    String getMarka() {
        return this.marka;
    }
    String getModel() {
        return this.model;
    }
    int getRokProdukcji () {
        return this.rokProdukcji;
    }

    @Override
    public String toString() {
        return "Samochod " +
                "marka='" + marka + '\'' +
                ", model='" + model + '\'' +
                ", rokProdukcji=" + rokProdukcji +
                '}';
    }
}

