public class Days {
    public static void main(String[] args) {
        String day = "piątek";
        switch (day) {
            case "poniedziałek":
            case "wtorek":
            case "środa":
            case "czwartek":
                System.out.println("Idziesz do roboty!");
                break;
            case "piątek":
                System.out.println("Już blisko!");
                break;
            case "sobota":
            case "niedziela":
                System.out.println("W końcu weekend");
                break;
            default:
                System.out.println("Dzień nierozpoznany");
        }
    }
}
