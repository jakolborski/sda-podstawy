import java.util.Locale;

public class KlasaString {
    public static void main(String[] args) {
        String text1 = "This is a test";
        String text2 = "This is a test";
        String text3 = new String("This is a test");

        //Operujemy na zawartości
        String val1 = text1.intern();
        String val2 = text2.intern();

        System.out.println(val1.equals(val2));

        System.out.println(text1.equals(text2)); //porównanie ciągu znaków
        System.out.println(text1 == text2); //porównanie zawartości pamięci

        System.out.println(text2.equals(text3));
        System.out.println(text2 == text3);

        String text4 = "My name is ";
        String text5 = "Kuba";
        String finalText = text4 + text5;
        System.out.println(finalText);

        String text6 = "This is ";
        String text7 = "a test";
        String finaltext2 =text6.concat(text7);
        System.out.println(finaltext2);

        System.out.println("Jak długi jest ten tekst?".length());
        String testValue = "This is test value";
        System.out.println(testValue.toUpperCase());
        System.out.println(testValue.toLowerCase());

        String testValue2 = "This is test value";
        System.out.println(testValue2.indexOf("is")); //Pokazuje 2, bo liczy od 0

        String text8 = "Hahaha! Funny joke!";
        System.out.println(text8.replaceAll("a", "o")); //Wielkość liter ma znaczenie
    }
}
