import java.util.Scanner;
import java.util.logging.SocketHandler;

public class Uzupelnianie {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Wpisz ilość wierszy");
        int wiersze = Integer.parseInt(scan.nextLine());
        System.out.println("Wpisz ilość kolumn");
        int kolumny = Integer.parseInt(scan.nextLine());
        System.out.println("Czy uzupełnić tabelę automatycznie?");
        String check = scan.nextLine();

        if (check.equals("tak")) {
            int[][] wielowymiarowa = new int[wiersze][kolumny];
            for (int wiersz = 0; wiersz < wiersze; wiersz++) {
                for (int kolumna = 0; kolumna < kolumny; kolumna++) {
                    wielowymiarowa[wiersz][kolumna] = ((int) (Math.random() * 100));
                    System.out.print(String.format("%3s", wielowymiarowa [wiersz][kolumna]));
                }
                System.out.println();
            }
        } else {
            System.out.println("\nUzupełnij tabele");
        }
    }
}
