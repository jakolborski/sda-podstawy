public class Metody {
    public static void main(String[] args) {
        int a = 5;
        int b = 6;

        int sum = dodawanie(a, b);
        System.out.println(sum);

        int dif = odejmowanie(a, b);
        System.out.println(dif);

        int multi = mnożenie(a, b);
        System.out.println(multi);

        int div = dzielenie(a, b);
        System.out.println(div);

    }

    static int dodawanie(int a, int b) {
        return a + b;
    }

    static int odejmowanie(int a, int b) {
        return a - b;
    }

    static int mnożenie(int a, int b) {
        return a * b;
    }

    static int dzielenie(int a, int b) {
        return a / b;
    }
}

