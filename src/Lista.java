import java.util.ArrayList;
import java.util.List;

public class Lista {
    public static void main(String[] args) {
        //Musi być typ Integer, a nie int
        List<Integer> listaRocznikow = new ArrayList<>();
        listaRocznikow.add(1959);
        listaRocznikow.add(1960);
        listaRocznikow.add(1970);
        for (Integer roczniki:listaRocznikow) {
            System.out.println(roczniki);
        }
        //Jeśli nie damy new Integer to usuniemy element o indeksie 1960
        listaRocznikow.remove(new Integer(1960));
        for (Integer roczniki:listaRocznikow) {
            System.out.println(roczniki);
        }
    }
}
