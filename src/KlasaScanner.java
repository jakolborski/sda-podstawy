import java.util.Locale;
import java.util.Scanner;

public class KlasaScanner {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in).useLocale(Locale.US);

        System.out.println("\nWpisz swoją liczbę: ");
        double usersNumber = scan.nextDouble();
        System.out.println(usersNumber);

        System.out.println("\nWpisz swój tekst: ");
        String textLine = scan.nextLine();
        System.out.println(textLine);
    }
}
