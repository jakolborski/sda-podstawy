public class Main {

    public static void main(String[] args) {
        Czlowiek czlowiek = new Czlowiek();
        czlowiek.setImie("Kuba");
        czlowiek.setNazwisko("Olborski");
        czlowiek.setWiek((short) 25);
        czlowiek.setWzrost(186);

        Czlowiek czlowiek2 = new Czlowiek();
        czlowiek2.setImie("Damian");
        czlowiek2.setNazwisko("Marszałek");
        czlowiek2.setWiek((short) 30);
        czlowiek2.setWzrost(201);

        String czlowiekImie = czlowiek.getImie();
        String czlowiekNazwisko = czlowiek.getNazwisko();
        int czlowiekWzrost = czlowiek.getWzrost();
        short czlowiekWiek = czlowiek.getWiek();
        System.out.printf("%s %s, %s, %s", czlowiekImie, czlowiekNazwisko, czlowiekWzrost, czlowiekWiek);

        String czlowiek2Imie = czlowiek2.getImie();
        String czlowiek2Nazwisko = czlowiek2.getNazwisko();
        int czlowiek2Wzrost = czlowiek2.getWzrost();
        short czlowiek2Wiek = czlowiek2.getWiek();
        System.out.printf("\n%s %s, %s, %s", czlowiek2Imie, czlowiek2Nazwisko, czlowiek2Wzrost, czlowiek2Wiek);
    }
}
